===========
xss-inhibit
===========

-----------------------------------------------------
execute a process while inhibiting the X screen saver
-----------------------------------------------------

:Author: Raymond Wagenmaker <raymondwagenmaker@gmail.com>
:Date: June 2014
:Manual section: 1
:Version: |version|

Synopsis
========

| xss-inhibit *command* [*arg*] ...
| xss-inhibit --version|--help

Description
===========

**xss-inhibit** executes *command* and keeps the X screen saver suspended until
it finishes.  This may be used to run non-X programs or X programs that lack
support for the MIT screen saver extension.

Options
=======

-h, --help  Print help message and exit.
--version   Print version number and exit.

Exit status
===========

**xss-inhibit** normally returns the exit status of the executed program.  If the
program was killed by a signal, the number of the signal plus 128 is returned.
If no program was executed, **xss-inhibit** itself generates an exit status:

125
    Generic error before executing the command.
126
    The command could not be executed.
127
    The command was not found.

See also
========

**xset**\(1)
