#compdef xss-inhibit

_arguments -S \
    '(- * :)--version[print version number and exit]' \
    '(- * :)'{-h,--help}'[print usage info and exit]' \
    '(-):command: _command_names -e' \
    '*::arguments: _normal'
