_xss-inhibit() {
    local cur prev words cword
    _init_completion || return

    local i
    for (( i=1; i <= cword; i++ )); do
        if [[ ${words[i]} != -* ]]; then
            _command_offset $i
            return
        elif [[ ${words[i]} == -- ]]; then
            _command_offset $(( i + 1 ))
            return
        fi
    done
    
    if [[ $cur == -* ]]; then
        COMPREPLY=( $(compgen -W '--version -h --help' -- $cur) )
    fi
}

complete -F _xss-inhibit xss-inhibit
