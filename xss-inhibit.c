/* Copyright (c) 2014 Raymond Wagenmaker
 *
 * See LICENSE for the MIT license.
 */

#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <getopt.h>
#include <assert.h>

#include <xcb/xcb.h>
#include <xcb/screensaver.h>

enum {
    EXIT_CANCELED = 125,
    EXIT_CANNOT_INVOKE = 126,
    EXIT_ENOENT = 127,
    EXIT__TERMSIG_BASE = 128
};

int
wait_for_child(const char *name, pid_t pid)
{
    int status;

    while (waitpid(pid, &status, 0) < 0) {
        if (errno != EINTR) {
            warn("Failed to wait for %s", name);
            return EXIT_CANCELED;
        }
    }
    if (WIFSIGNALED(status))
        return EXIT__TERMSIG_BASE + WTERMSIG(status);

    if (WIFEXITED(status))
        return WEXITSTATUS(status);

    warnx("%s failed due to unknown reason", name);
    return EXIT_CANCELED;
}

void
parse_options(int argc, char *argv[])
{
    enum {
        ARG_VERSION = 0x100
    };

    static const struct option options[] = {
        {"help",    no_argument, NULL, 'h'},
        {"version", no_argument, NULL, ARG_VERSION},
        {NULL}
    };

    int opt_char;

    while ((opt_char = getopt_long(argc, argv, "+h", options, NULL)) != -1) {
        switch (opt_char) {

        case ARG_VERSION:
            printf("version"); // FIXME
            exit(EXIT_SUCCESS);

        case 'h':
            printf("Usage: %s COMMAND [ARGUMENT...]\n\n"
                   "Execute a process while inhibiting the X screen saver.\n\n"
                   "  -h, --help        Print this help and exit\n"
                   "      --version     Print version information and exit\n",
                   argv[0]);
            exit(EXIT_SUCCESS);

        case '?':
            exit(EXIT_CANCELED);

        default:
            assert(!"Unhandled option");
        }
    }

    if (optind >= argc)
        errx(EXIT_CANCELED, "No command specified\n"
                            "Try --help or -h for usage information.");
}

int
main(int argc, char *argv[])
{
    xcb_connection_t *connection = NULL;
    pid_t child;
    int exit_status;

    parse_options(argc, argv);

    connection = xcb_connect(NULL, NULL);
    if (xcb_connection_has_error(connection))
        errx(EXIT_CANCELED, "Connecting to X server failed");

    xcb_screensaver_suspend(connection, true);

    switch (child = fork()) {

    case 0:
        execvp(argv[optind], argv + optind);
        warn("Failed to execute %s", argv[optind]);
        _exit(errno == ENOENT ? EXIT_ENOENT : EXIT_CANNOT_INVOKE);

    case -1:
        warn("Failed to fork");
        exit_status = EXIT_CANCELED;
        break;

    default:
        exit_status = wait_for_child(argv[optind], child);
        break;
    }

    xcb_screensaver_suspend(connection, false);

    xcb_disconnect(connection);
    exit(exit_status);
}
